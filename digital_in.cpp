#include <avr/io.h>
#include "digital_in.h"

Digital_in::Digital_in (uint8_t a) { 
    pinMask  = a;         // Access specifier
}
void Digital_in::init() {  // Method/function defined inside the class
    DDRB &= ~(1<<pinMask);
    
    };
  
bool Digital_in::is_hi() {  // Method/function defined inside the class
    return ((PINB>>pinMask) & 1 );
}

bool Digital_in::is_lo() {  // Method/function defined inside the class
    return !(PINB & (1<<pinMask));
} 
