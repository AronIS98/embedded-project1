

///////////////////////////////     PART 1     /////////////////////////////// 

#include <avr/interrupt.h>
#include <util/delay.h>
#include "digital_out.h"
#include "digital_in.h"
#include "encoder.h"

int main()
{
  Digital_out led(5);   // PB5 Arduino Nano built-in LED 
  led.init();
  Encoder encoder;
  Digital_out motor(1);
  Digital_in enc1(3); //Replace with encoder pin 1
  Digital_in enc2(4); //Replace with encoder pin 2

  int counter = 0;
  led.set_lo();
  motor.set_lo();
  while (1)
  {
    


      encoder.read_encoder(enc1.is_hi(),enc2.is_hi());
      counter = encoder.position();
      _delay_us(300);

    if (counter >= 10) 
    {
      led.set_hi();
      motor.set_hi();
    }

  }
}
  

  
// ///////////////////////////////     PART 2     /////////////////////////////// 
// #include <avr/interrupt.h>
// #include <util/delay.h>
// #include "digital_out.h"
// #include "digital_in.h"
// #include "encoder.h"


// volatile int enc_position = 0;
// int main()
// {
//   Digital_out led(5);
//   Digital_out motor(1);
//   Encoder enc;
//   enc.init();
//   led.set_lo();
//   motor.set_lo();
//   while(1)
//   {

//     if(enc_position>=200){led.set_hi(); motor.set_hi();}
//     // else{led.set_lo(); motor.set_lo();}
    
    
//   }
//   return 1;
// }
// ISR(INT0_vect)
// {
//   DDRD &= ~(1<<3);
//   if(((PIND>>3) & 1 )){enc_position++;}
//   else{enc_position--;}

// }
